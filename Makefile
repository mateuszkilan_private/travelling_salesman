CC=$(CXX)

all: Main.o PointsHelpers.o
	$(CXX) Main.o PointsHelpers.o -o Mateusz_Kilan_komiwojazer_program 

Main.o: Main.cpp
	$(CXX) -std=c++14 -c Main.cpp

PointsHelpers.o: PointsHelpers.cpp
	$(CXX) -std=c++14 -c PointsHelpers.cpp

clean:
	rm -rf *.o
