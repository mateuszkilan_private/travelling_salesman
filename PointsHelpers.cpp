#include "PointsHelpers.h"
#include <iostream>

void PointsHelpers::getNoOfPointsFromInput(int& noOfPoints) {
    string noOfPointsString;
    cout << "\nPlease type in number of points: ";
    cin >> noOfPointsString;

    while (true) {
        if (isStringValidNaturalNumberExcludingZero(noOfPointsString)) {
            noOfPoints = stoi(noOfPointsString);
            return;
        } else {
            cout << "Invalid number format, please try again: ";
            cin >> noOfPointsString;
        }
    }
}

bool PointsHelpers::isStringValidNaturalNumberExcludingZero(const string noOfPointsString) {
    unsigned int stringSize = noOfPointsString.size();

    if (
        stringSize == 0 ||
        stringSize >= 1 && noOfPointsString[0] == '0'
    ) {
        return false;
    }

    for (unsigned int i = 0; i < stringSize; i++) {
        switch (noOfPointsString[i]) {
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                break; //everything is ok 
            default:
                return false;
        }
    }

    return true;
}

bool PointsHelpers::isStringValidIntegerNumber(const string noOfPointsString) {
    unsigned int stringSize = noOfPointsString.size();

    // cout << "\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    // cout << "\nnoOfPointsString = " << noOfPointsString;
    // cout << "\nsize = " << noOfPointsString.size();
    // cout << "\n[0] = " << noOfPointsString[0];
    // cout << "\n[1] = " << noOfPointsString[1];
    // cout << "\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";

    if (
        stringSize == 0 ||
        stringSize == 1 && noOfPointsString[0] == '-' ||
        stringSize > 1 && noOfPointsString[0] == '0' ||
        stringSize > 1 && noOfPointsString[0] == '-' && noOfPointsString[1] == '0'
    ) {
        return false;
    }

    bool isPotentiallyNegativeNumber = noOfPointsString[0] == '-';

    for (unsigned int i = isPotentiallyNegativeNumber ? 1 : 0; i < stringSize; i++) {
        switch (noOfPointsString[i]) {
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                break; //everything is ok 
            default:
                return false;
        }
    }

    return true;
}

void PointsHelpers::getPointsCoordinatesFromInput(int& noOfPoints, PointsVector& notVisitedPoints) {
    string xCoord = "";
    string yCoord = "";
    for (int i = 0; i < noOfPoints; i++) {
        cout << "\nPlease type in " << i + 1 << " point's coordinates: ";

        cout << "\nx = ";
        cin >> xCoord;
        while (!isStringValidIntegerNumber(xCoord)) {
            cout << "Invalid x coordinate number, please try again: ";
            cin >> xCoord;
        }

        cout << "y = ";
        cin >> yCoord;
        while (!isStringValidIntegerNumber(yCoord)) {
            cout << "Invalid y coordinate number, please try again: ";
            cin >> yCoord;
        }

        notVisitedPoints[i] = Point(stoi(xCoord), stoi(yCoord), i + 1);
    }
}

void PointsHelpers::getStartingPointFromInput(PointsVector& notVisitedPoints, PointsVector& visitedPoints, int& startingPointId) {
    string startingPointNo = "";

    cout << "\nPlease choose starting point: ";

    for (PointsVector::const_iterator iter = notVisitedPoints.cbegin(); iter != notVisitedPoints.cend(); iter++) {
        cout << "\nPoint " << (*iter).id << " = { x: " << (*iter).x << ", y: " << (*iter).y << " }";
    }

    cout << '\n';
    cin >> startingPointNo;
    while (!startingPointFormatIsValid(startingPointNo, notVisitedPoints.size())) {
        cout << "Invalid starting point number format, please try again: ";
        cin >> startingPointNo;
    }

    startingPointId = stoi(startingPointNo);

    visitedPoints.emplace_back(notVisitedPoints[startingPointId - 1]);
    notVisitedPoints.erase(notVisitedPoints.begin() + startingPointId - 1);
}

bool PointsHelpers::startingPointFormatIsValid(const string startingPointNo, size_t notVisitedPointsSize) {
    unsigned int stringSize = startingPointNo.size();

    if (
        stringSize == 0 ||
        stringSize >= 1 && startingPointNo[0] == '0'
    ) {
        return false;
    }

    for (unsigned int i = 0; i < stringSize; i++) {
        switch (startingPointNo[i]) {
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                break; //everything is ok 
            default:
                return false;
        }
    }

    int number = stoi(startingPointNo);

    if (number > 0 && number <= notVisitedPointsSize) {
        return true;
    }

    return false;
}

void PointsHelpers::DEBUG_ShowVector(PointsVector& pointsVec, string vectorsDescription) {
    cout << "\nXXXXXXXXXXXXXXXXX " << vectorsDescription << " XXXXXXXXXXXXXXXXXX";
    for (PointsVector::const_iterator iter = pointsVec.cbegin(); iter != pointsVec.cend(); iter++) {
        cout << "\nPoint " << (*iter).id << " = { x: " << (*iter).x << ", y: " << (*iter).y << " }";
    }
    cout << "\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
}