#include <iostream>
#include "PointsHelpers.h"

/*
Zalozenia programu:
Program powinien przyjmowac na wejsciu dowolna ilosc argumentow punktow.
*/

int main () {
    using namespace PointsHelpers;

    int noOfPoints = 0;
    int startingPointId = 0;
    PointsVector visitedPoints = {};
    PointsVector notVisitedPoints = {};
    PointsVector allPoints = {};

    cout << "\nWelcome in travelling salesman problem solver program made by Mateusz Kilan!";

    getNoOfPointsFromInput(noOfPoints);
    notVisitedPoints.resize(noOfPoints);

    getPointsCoordinatesFromInput(noOfPoints, notVisitedPoints);
    allPoints = notVisitedPoints;
    getStartingPointFromInput(notVisitedPoints, visitedPoints, startingPointId);

    DEBUG_ShowVector(notVisitedPoints, "notVisitedPoints");
    DEBUG_ShowVector(visitedPoints, "visitedPoints");
    DEBUG_ShowVector(allPoints, "allPoints");

    return 0;
}
