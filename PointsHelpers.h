#pragma once
#include <vector>
#include <string>

struct Point {
    Point() : x(0), y(0), id(0) {}
    Point(int xCoord, int yCoord, int idNo) : x(xCoord), y(yCoord), id(idNo) {}
    int x;
    int y;
    int id;
};

using PointsVector = std::vector<Point>;
using namespace std;

namespace PointsHelpers {
    extern bool isStringValidIntegerNumber(const string noOfPointsString);
    extern bool isStringValidNaturalNumberExcludingZero(const string isStringValid);
    extern bool startingPointFormatIsValid(const string startingPointNo, size_t notVisitedPointsSize);

    extern void getNoOfPointsFromInput(int& noOfPoints);
    extern void getPointsCoordinatesFromInput(int& noOfPoints, PointsVector& notVisitedPoints);
    extern void getStartingPointFromInput(PointsVector& notVisitedPoints, PointsVector& visitedPoints, int& startingPointId);

    extern void DEBUG_ShowVector(PointsVector& pointsVec, string vectorsDescription);
}